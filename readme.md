Web application that returns information about the current user's browser using the JavaScript Navigator object.

I was curious to find out how many details about a user that a browser can summarize just from the one page.

I plan on extending this application beyond the navigator JS object when I get the time.