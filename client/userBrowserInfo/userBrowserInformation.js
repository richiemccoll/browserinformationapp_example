Template.userBrowserInformation.onRendered(function(){
var applicationName = navigator.appCodeName;
document.getElementById("applicationName").innerHTML = applicationName; 
var applicationVersion = navigator.appVersion;
document.getElementById("applicationVersion").innerHTML = applicationVersion;
var platform = navigator.platform;
document.getElementById("applicationPlatform").innerHTML = platform;
var userAgent = navigator.userAgent;
document.getElementById("userAgent").innerHTML = userAgent;
var javaEnabled = navigator.javaEnabled();
document.getElementById("javaEnabled").innerHTML = javaEnabled;
var screenWidth = screenWidth();
function screenWidth() {
if (window.screen){
return(screen.width);}
else {return(0);}
}
document.getElementById("screenWidth").innerHTML = screenWidth; 
var screenHeight = screenHeight();
function screenHeight() {
if (window.screen) {
return(screen.height);
} else {
return(0);
}  
}
document.getElementById("screenHeight").innerHTML = screenHeight;
var browserLanguage = navigator.language;
document.getElementById("browserLanguage").innerHTML = browserLanguage;  
});